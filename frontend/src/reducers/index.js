import { combineReducers } from "redux";
import homeReducer from "../home/reducers/homeReducer";
import orderReducer from "../order/reducers/orderReducer";
import addReducer from "../add/reducers/addReducer";

const reducers = combineReducers({
  homeReducer,
  orderReducer,
  addReducer,
});
export default reducers;
