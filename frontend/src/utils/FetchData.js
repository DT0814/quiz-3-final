//TODO: format this file
export const
  fetchDataByPost = (url, data) => {
  return fetch(url, {
    method: 'POST',
    headers: {
      'content-type': 'application/json;charset=UTF-8'
    },
    body: data
  }).then(response => {
    //TODO: remove console.log
    console.log(response);
    return response.ok;
  })
};

export const fetchDataByGet = (url) => {
  return fetch(url, {
    method: 'GET',
  }).then(response => response.json())
};
export const fetchDataByDelete = (url) => {
  return fetch(url, {
    method: 'DELETE',
  }).then(response => {
    //TODO: remove console.log
    console.log(response);
    return response.ok;
  })
};
