const initState = {
  goods: [],
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'GET_ALL_GOODS':
      return {
        ...state,
        goods: action.goods
      };
    case  'ADD_ITEM':
      //TODO: use ===
      //TODO: 为啥要在这个地方alert？？？不是应该在请求发成功的successCallback里面吗？
      if(action.res == 201){
        alert("添加成功");
      }
      return  {
        ...state,
      };
    default:
      return state
  }
};
