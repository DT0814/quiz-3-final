import React from 'react'

class Show extends React.Component {

  addGoodsItem(event){
    //TODO: please delete console.log
    console.log(event.target.value);
    this.props.addItem(event.target.value);
  }

  render() {
    const goods = this.props.goods;
    return (
      <div className='show-body'>
        {
          goods.map(((value, index) => {
            return (
              <div key={'div'+index} className='home-item-div'>
                <img key={'img'+index} src={'http://localhost:8080/' + value.imgUrl}/>
                <p key={'p-name'+index}>{value.name}</p>
                <p key={'p-price'+index}>{value.price}元/{value.unit}</p>
                <button key={'button'+index} onClick={this.addGoodsItem.bind(this)} value={value.id}>+</button>
                <br key={'br'+index}/>
              </div>
            )
          }))
        }
      </div>
    );
  }
}

export default Show;
