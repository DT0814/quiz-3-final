import React from 'react'
import './home.less'
import { bindActionCreators } from "redux";
import { addItem, getAll } from "../actions/homeActions";
import { connect } from "react-redux";
import Show from "../componets/Show";
export class Home extends React.Component {

  componentDidMount() {
    this.props.getAll();
  }

  render() {
    //TODO: const {addItem, homeReducer} = this.props;
    return (
      <div className={'home-body'}>
        <Show addItem={this.props.addItem} goods={this.props.homeReducer.goods}/>
      </div>
    ) //TODO: 加分号
  }
}

const mapStateToProps = state => ({
  //TODO: 不用加定义props名称时不用在后面加reducer
  homeReducer: state.homeReducer
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    getAll,
    addItem
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);

