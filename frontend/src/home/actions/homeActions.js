import { fetchDataByGet, fetchDataByPost } from "../../utils/FetchData";

export const getAll = () => (dispatch) => {
  return fetchDataByGet('/api/goods')
    .then(result => {
      dispatch({
        type: 'GET_ALL_GOODS',
        goods: result
      });
    })
};
export const addItem = (goodsId) => (dispatch) => {
  //TODO: data = {goods_id: goodsId}
  let data = {};
  data["goods_id"] = goodsId;
  return fetchDataByPost('/api/order/item', JSON.stringify(data))
  //TODO: 不需要result
    .then(result => {
      dispatch({
        type: 'ADD_ITEM',
        //TODO: 这个201什么意思？
        result: 201
      });
    })
};
