import React, { Component } from 'react';
import './App.less';
//TODO: unused import
import { BrowserRouter as Router, Link, NavLink } from "react-router-dom";
import { Route, Switch } from "react-router";
import Home from "./home/page/Home";
import Order from "./order/page/Order";
import Add from "./add/page/Add";

class App extends Component {

  render() {
    return (
      <Router>
        <div className='header-div'>
          <nav>
            <ul>
              <li>
                <NavLink exact activeClassName={'selected'} to={'/'}>商城</NavLink>
              </li>
              <li>
                <NavLink activeClassName={'selected'} to={'/order'}>订单</NavLink>
              </li>
              <li>
                <NavLink activeClassName={'selected'} to={'/add'}>添加商品</NavLink>
              </li>
            </ul>
          </nav>
        </div>
        <Switch>
          <Route exact path={'/'} component={Home}/>
          <Route path={'/order'} component={Order}/>
          <Route path={'/add'} component={Add}/>
        </Switch>
      </Router>
    );
  }
}

export default App;
