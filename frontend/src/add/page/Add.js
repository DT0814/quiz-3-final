import React from 'react'
import './add.less'
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { addGoods } from "../actions/addActions";

export class Add extends React.Component {
  componentDidMount() {
  }

  createGoods(event) {
    event.preventDefault();
    const formData = new FormData(event.target);
    let data = {};

    for (const [key, value]  of formData.entries()) {
      data[key] = value;
    }
    console.log(JSON.stringify(data));
    this.props.addGoods(JSON.stringify(data));
  }

  render() {
    return (
      <div className={'add-body'}>
        <form onSubmit={this.createGoods.bind(this)}>
          <label htmlFor="add-name">名称</label>
          <input type="text" name='name' id='add-name'/>
          <label htmlFor="add-price">价格</label>
          <input type="text" name='price' id='add-price'/>
          <label htmlFor="add-unit">单位</label>
          <input type="text" name='unit' id='add-unit'/>
          <label htmlFor="add-img-url">图片</label>
          <input type="text" name='imgUrl' id='add-img-url'/>
          <input type="submit" value='提交'/>
        </form>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  addReducer: state.addReducer
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    addGoods
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Add);

