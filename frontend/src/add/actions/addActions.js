import { fetchDataByGet, fetchDataByPost } from "../../utils/FetchData";

export const addGoods = (data) => (dispatch) => {

  return fetchDataByPost('/api/goods',data)
    .then(result => {
      console.log(result);
      dispatch({
        type: 'ADD_GOODS',
        result: true
      });
    })
};
