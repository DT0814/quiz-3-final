const initState = {
  orders: [],
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'GET_ALL_ORDERS':
      return {
        ...state,
        orders: action.orders
      };
    case 'DELETE_ITEM':
      if (action.res){
        alert("删除成功")
      }
      return {
        ...state,
      };
    default:
      return state
  }
};
