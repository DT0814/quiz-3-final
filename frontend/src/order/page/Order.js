import React from 'react'
import './order.less'
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { deleteItem, getAll } from "../actions/orderActions";
import Show from "../componets/Show";
export class Order extends React.Component {

  componentDidMount() {
    this.props.getAll();
  }

  render() {
    //TODO: remove console.log
    console.log(this.props.orders);
    return (
      <div className={'home-body'} >
        <Show orders={this.props.orderReducer.orders} deleteItem={this.props.deleteItem}/>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  orderReducer: state.orderReducer
});

const mapDispatchToProps = dispatch => bindActionCreators(
  {
    getAll,
    deleteItem
  }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Order);

