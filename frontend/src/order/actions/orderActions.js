import { fetchDataByDelete, fetchDataByGet } from "../../utils/FetchData";

export const getAll = () => (dispatch) => {
  return fetchDataByGet('/api/order/item')
    .then(result => {
      //TODO: remove conosle.log
      console.log(result);
      dispatch({
        type: 'GET_ALL_ORDERS',
        orders: result
      });
    })
};
export const deleteItem = (id) => (dispatch) => {
  return fetchDataByDelete('/api/order/item/'+id)
    .then(result => {
      //TODO: remove conosle.log
      console.log(result);
      dispatch({
        type: 'DELETE_ITEM',
        res: true
      });
    })
};
