import React from 'react'

class Show extends React.Component {

  deleteItem(event) {
    this.props.deleteItem(event.target.value);
  }

  render() {

    const orders = this.props.orders;

    return (
      <div className='order-show-body'>
        <table>
          <tr>
            <th>名字</th>
            <th>单价</th>
            <th>数量</th>
            <th>单位</th>
            <th>操作</th>
          </tr>
          {
            //TODO: remove index
            orders.map(((value, index) => {
              //TODO: const {name, price, num, unist} = value.goods;
              return (
                <tr>
                  <td>{value.goods.name}</td>
                  <td>{value.goods.price}</td>
                  <td>{value.num}</td>
                  <td>{value.goods.unit}</td>
                  <td>
                    <button onClick={this.deleteItem.bind(this)} value={value.id}>删除</button>
                  </td>
                </tr>
              )
            }))
          }


        </table>
      </div>
    );
  }
}

export default Show;
