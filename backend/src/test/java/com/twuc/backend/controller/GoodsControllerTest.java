package com.twuc.backend.controller;

import com.twuc.backend.ApiTestBase;
import com.twuc.backend.entity.Goods;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class GoodsControllerTest extends ApiTestBase {

    //TODO: 你这个测试不够吧。。。
    @Test
    void should_return_200_when_get_all_goods() throws Exception {
        //Arrange

        //TODO: act and assert
        //Act
        mockMvc.perform(get("/api/goods")).andExpect(status().isOk());
        //Assert
    }

    @Test
    void should_return_201_when_get_all_goods() throws Exception {
        //Arrange
        //TODO: act and assert
        //Act
        mockMvc.perform(
                post("/api/goods")
                        .contentType(MediaType.APPLICATION_JSON).content(serialize(new Goods("鸭梨","个",10,"yali.jpg"))))
                .andExpect(status().isCreated());
        //Assert
    }
}
