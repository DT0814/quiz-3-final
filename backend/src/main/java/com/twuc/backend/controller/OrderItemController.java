package com.twuc.backend.controller;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.twuc.backend.service.OrderItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author tao.dong
 */
@RestController
@RequestMapping("/api/order/item")
public class OrderItemController {
    private OrderItemService service;

    public OrderItemController(OrderItemService service) {
        this.service = service;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteItem(@PathVariable Long id) {
        service.deleteItem(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    public ResponseEntity getAll() {
        return ResponseEntity.ok(service.findAll());
    }

    @PostMapping
    private ResponseEntity add(@RequestBody ItemRequest item) {

        service.addItem(item.goodsId);

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    //TODO: move to ItemRequest class
    static class ItemRequest {
        @JsonProperty("goods_id")
        private Long goodsId;

        public Long getGoodsId() {
            return goodsId;
        }

        public void setGoodsId(Long goodsId) {
            this.goodsId = goodsId;
        }

        public ItemRequest() {
        }
    }
}
