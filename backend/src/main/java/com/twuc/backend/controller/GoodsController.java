package com.twuc.backend.controller;

import com.twuc.backend.dao.OrderRepository;
import com.twuc.backend.entity.Goods;
//TODO: unused import
import com.twuc.backend.entity.Orders;
import com.twuc.backend.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

//TODO: remove this
/**
 * @author tao.dong
 */
@RestController
@RequestMapping("/api/goods")
public class GoodsController {

    private GoodsService goodsService;
    //TODO: field injection is not recommended
    @Autowired
    OrderRepository repository;
    public GoodsController(GoodsService goodsService) {
        this.goodsService = goodsService;
    }

    @GetMapping()
    public ResponseEntity<List<Goods>> getAll() {
        List<Goods> res = goodsService.findAll();
        return ResponseEntity.ok(res);
    }

    @PostMapping
    public ResponseEntity add(@RequestBody @Valid Goods goods) {
        System.out.println(goods);
        goodsService.addGoods(goods);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
