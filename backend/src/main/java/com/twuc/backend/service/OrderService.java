package com.twuc.backend.service;

import com.twuc.backend.dao.OrderRepository;
import com.twuc.backend.entity.Orders;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * @author tao.dong
 */
@Service
public class OrderService {

    private OrderRepository repository;

    public OrderService(OrderRepository repository) {
        this.repository = repository;
    }
    @PostConstruct
    private void init(){
        if(!repository.findById(1L).isPresent()){
            repository.saveAndFlush(new Orders());
        }
    }
}
