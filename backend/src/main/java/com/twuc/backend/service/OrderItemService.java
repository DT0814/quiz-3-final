package com.twuc.backend.service;

import com.twuc.backend.dao.GoodsRepository;
import com.twuc.backend.dao.OrderItemRepository;
import com.twuc.backend.dao.OrderRepository;
import com.twuc.backend.entity.OrderItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author tao.dong
 */
@Service
public class OrderItemService {

    private final OrderItemRepository repository;
    private final GoodsRepository goodsRepository;
    private final OrderRepository orderRepository;

    public OrderItemService(OrderItemRepository repository, GoodsRepository goodsRepository, OrderRepository orderRepository) {
        this.repository = repository;
        this.goodsRepository = goodsRepository;
        this.orderRepository = orderRepository;
    }

    public void addItem(Long goodsId) {
        OrderItem orderItem = new OrderItem();
        //TODO: need to check ifPresent
        orderItem.setGoods(goodsRepository.findById(goodsId).get());
        //TODO: need to check ifPresent
        //TODO: hard coded order id
        orderItem.setOrders(orderRepository.findById(1L).get());
        List<OrderItem> res = repository.findAll();
        System.out.println(res);
        if (CollectionUtils.isEmpty(res)){
            orderItem.setNum(1);
            repository.saveAndFlush(orderItem);
        }else{
            for (OrderItem item : res) {
                if(item.getGoods().getId().equals(goodsId)){
                    item.setNum(item.getNum()+1);
                    repository.saveAndFlush(item);
                    return;
                }
            }
            orderItem.setNum(1);
            repository.saveAndFlush(orderItem);
        }
    }

    public List<OrderItem> findAll() {
        return repository.findAll();
    }

    public void deleteItem(Long id) {
        repository.deleteById(id);
    }
}
