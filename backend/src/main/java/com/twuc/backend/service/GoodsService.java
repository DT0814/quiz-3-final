package com.twuc.backend.service;

import com.twuc.backend.dao.GoodsRepository;
import com.twuc.backend.entity.Goods;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author tao.dong
 */
@Service
public class GoodsService {
    private GoodsRepository repository;

    public GoodsService(GoodsRepository repository) {
        this.repository = repository;
    }

    public List<Goods> findAll() {
        List<Goods> all = repository.findAll();
        System.out.println(all);
        return all;
    }

    public void addGoods(Goods goods) {
        repository.saveAndFlush(goods);
    }
}
