package com.twuc.backend.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author tao.dong
 */
@Entity
@Table(name = "goods")
public class Goods {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @NotNull
    private String name;

    @Column
    @NotNull
    private String unit;

    @Column
    @NotNull
    private Integer price;

    //TODO: 应该不需要写这个name吧？
    @Column(name = "img_url")
    @NotNull
    private String imgUrl;


    public Goods() {
    }

    public Goods(String name, String unit, Integer price, String imgUrl) {
        this.name = name;
        this.unit = unit;
        this.price = price;
        this.imgUrl = imgUrl;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUnit() {
        return unit;
    }

    public Integer getPrice() {
        return price;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    @Override
    public String toString() {
        return "Goods{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", unit='" + unit + '\'' +
                ", price=" + price +
                ", imgUrl='" + imgUrl + '\'' +
                '}';
    }
}
