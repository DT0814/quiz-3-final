package com.twuc.backend.entity;

import javax.persistence.*;

/**
 * @author tao.dong
 */
@Entity
@Table(name = "order_item")
public class OrderItem {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private Integer num;

    //TODO: 不用写括号
    @ManyToOne()
    @JoinColumn(name = "goods_id")
    private Goods goods;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private Orders orders;

    public OrderItem() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public Orders getOrders() {
        return orders;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }

    @Override
    public String
    toString() {
        return "OrderItem{" +
                "id=" + id +
                ", num=" + num +
                ", goods=" + goods +
                ", orders=" + orders +
                '}';
    }
}

