package com.twuc.backend.dao;

import com.twuc.backend.entity.Goods;
import com.twuc.backend.entity.OrderItem;
import com.twuc.backend.entity.Orders;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface OrderItemRepository extends JpaRepository<OrderItem, Long> {
}
