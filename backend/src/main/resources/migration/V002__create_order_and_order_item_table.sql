-- TODO: 为啥两个表放在一个migration文件中？
CREATE TABLE IF NOT EXISTS `orders`
(
    `id` BIGINT AUTO_INCREMENT PRIMARY KEY
) DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS `order_item`
(
    `id`       BIGINT AUTO_INCREMENT PRIMARY KEY,
    `num`      int    NOT NULL,
    `order_id` BIGINT NOT NULL,
    `goods_id` BIGINT NOT NULL,
    foreign key (`order_id`) references `orders` (`id`),
    foreign key (`goods_id`) references goods (`id`)
) DEFAULT CHARSET = utf8;
