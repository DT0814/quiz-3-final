CREATE TABLE IF NOT EXISTS goods
(
    `id`   BIGINT AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(64) NOT NULL,
    `unit` VARCHAR(64) NOT NULL,
    `price` int(11) NOT NULL,
    `img_url` VARCHAR(64) NOT NULL
)DEFAULT CHARSET=utf8;
